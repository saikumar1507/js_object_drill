function invert(obj) {
    let objectInvert = {}
    for (let [key, value] of Object.entries(obj)) {
        objectInvert[value]=key
    }
    return objectInvert
}
module.exports = invert