const testObject = require('./testObject.js')

const keysList = Object.keys(testObject)

const pass = null

function defaults(obj, defaultProps) {
    for (let [key, val] of Object.entries(defaultProps)) {
        if (keysList.includes(key)) {
            pass
        } else {
            obj[key] = val
        }
    }
    return obj
}

module.exports = defaults