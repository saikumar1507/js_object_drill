const testObject = require('./testObject.js')

function mapObject(testObject, cb) {
    let temp = {}
    for (let [key, value] of Object.entries(testObject)) {
        temp[key] = cb([key])
    }
    return temp
}
module.exports = mapObject