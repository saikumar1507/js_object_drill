const testObject = require('../testObject.js')

const invert = require('../invert.js')

const Expected_output = { '36': 'age', 'Bruce Wayne': 'name', 'Gotham': 'location' }

const Actual_output = invert(testObject)


if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log('Inverted object is : ', Actual_output)
} else {
    console.log('Object is not inverted')
}