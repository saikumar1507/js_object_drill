const testObject = require('../testObject.js')

const defaultProps = { 'name': 'ThomosWayne ', 'age': 57, 'location': 'USA', 'gender': 'male', 'nationality': 'American' }

const defaults = require('../defaults.js')

const Actual_output = defaults(testObject, defaultProps)

const Expected_output = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    gender: 'male',
    nationality: 'American'
}

if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log(Actual_output)
} else {
    console.log('Wrong Answer')
}