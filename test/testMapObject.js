const testObject = require('../testObject.js')

const mapObject = require('../mapObject.js')

function cb([key]) {

    if (key == 'name') {
        return testObject[key] = 'Thomas Wayne'
    } else if (key == 'age') {
        return testObject[key] = 57
    } else if (key == 'location') {
        return testObject[key] = 'America'
    }
}
const Actual_output = mapObject(testObject, cb)

const Expected_output = { name: 'Thomas Wayne', age: 57, location: 'America' }

if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log(Actual_output)
} else {
    console.log('wrong answer')
}