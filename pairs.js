function pairs(obj) {
    let keyValueArray = []
    for (let [key, value] of Object.entries(obj)) {
        keyValueArray.push([key, value])
    }
    return keyValueArray
}
module.exports = pairs